﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace MP4Player
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<TimeSpan> markers = new List<TimeSpan>();
        public string Path { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenVideoButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Video";
            dlg.DefaultExt = ".mp4";
            dlg.Filter = "Video file (.mp4)|*.mp4";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                Path = dlg.FileName;
                mediaControl.Source = new Uri($"{ Path}");
            }
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            mediaControl.Play();
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            mediaControl.Pause();
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            mediaControl.Stop();
        }

        private void AddMarkerButton_Click(object sender, RoutedEventArgs e)
        {
            markers.Add(mediaControl.Position);            
        }

        private void MoveToPrevMarkerButton_Click(object sender, RoutedEventArgs e)
        {
            if (markers.Count != 0)
            {
                var nextClosest = markers.Where(x => mediaControl.Position < x).FirstOrDefault();
                if (nextClosest != null)
                {
                    mediaControl.Position = nextClosest;
                }
            }
        }

        private void MoveToNextMarkerButton_Click(object sender, RoutedEventArgs e)
        {
            if (markers.Count != 0)
            {
                var nextClosest = markers.Where(x => mediaControl.Position > x).FirstOrDefault();
                if (nextClosest != null)
                {
                    mediaControl.Position = nextClosest;
                }
            }
        }
    }
}
